import unittest
from pathlib import Path
from turandot.model.config.config import ConfigModel
from turandot.model.datastructures.cslasset import CslAsset
from turandot.model.datastructures.tmplasset import TemplateAsset


class TestDbAsset(unittest.TestCase):

    @classmethod
    def setUpClass(cls) -> None:
        pass

    @classmethod
    def tearDownClass(cls) -> None:
        pass

    def test_001_create_get_delete_assets(self):
        tmpllist = TemplateAsset.get_all()
        csllist = CslAsset.get_all()
        self.assertEqual(0, len(tmpllist))
        self.assertEqual(0, len(csllist))
        new_tmpl = TemplateAsset(path="/tmp/test.zip", allow_mako=True, allow_jinja=False)
        new_tmpl.save()
        new_csl = CslAsset(path="/tmp/test.csl")
        new_csl.save()
        self.assertEqual(new_tmpl.dbid, 1)
        self.assertEqual(new_csl.dbid, 1)
        get_tmpl = TemplateAsset.get(dbid=1)
        get_csl = CslAsset.get(dbid=1)
        self.assertEqual(get_csl.dbid, 1)
        self.assertEqual(get_csl.path, Path("/tmp/test.csl"))
        self.assertEqual(get_tmpl.dbid, 1)
        self.assertEqual(get_tmpl.path, Path("/tmp/test.zip"))
        self.assertEqual(get_tmpl.allow_mako, True)
        self.assertEqual(get_tmpl.allow_jinja, False)
        tmpllist = TemplateAsset.get_all()
        csllist = CslAsset.get_all()
        self.assertEqual(len(tmpllist), 1)
        self.assertEqual(len(csllist), 1)
        new_tmpl.delete()
        new_csl.delete()
        tmpllist = TemplateAsset.get_all()
        csllist = CslAsset.get_all()
        self.assertEqual(0, len(tmpllist))
        self.assertEqual(0, len(csllist))
