# import unittest
import traceback
from tkinter import *
from tkinter import ttk
import time
import threading

from turandot.ttk.presentations import KvCombobox


class TestGui:
    def __init__(self):
        root = Tk()
        root.title("Test GUI")
        mainframe = ttk.Frame(root, padding="3 3 12 12")
        mainframe.grid(column=0, row=0, sticky=(N, W, E, S))
        self.combo = KvCombobox(mainframe, state="readonly")
        self.combo.grid(column=0, row=0, sticky=W)
        self.combo.set_callback(self.callback)
        self.combo.add_option(1, "testoption")
        self.combo.add_option(2, "testoption")
        self.combo.current(0)
        root.mainloop()

    def callback(self, *args):
        print(self.combo.get())


# class TestTtkErrorDialog(unittest.TestCase):
#
#     def test_show_dialog(self):
#         t = TestGui()


if __name__ == "__main__":
    t = TestGui()
