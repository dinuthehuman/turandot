#!/usr/bin/env bash

python{PYMAJ}.{PYMIN} -m venv {VENVPATH}
. {VENVPATH}/bin/activate
pip3 install --upgrade pip

# We need the wheel package to be able to export whl files
pip3 install wheels

pip3 install --no-cache-dir turandot[gtk]
deactivate
