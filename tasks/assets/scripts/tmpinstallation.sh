#!/usr/bin/env bash

mkdir /tmp/turandotbuild
python{PYMAJ}.{PYMIN} -m venv /tmp/turandotbuild/venv
. /tmp/turandotbuild/venv/bin/activate
pip3 install --upgrade pip

# We need the wheel package to be able to export whl files
pip3 install wheels

# Not really sure why tkhtml does not pull in it's own dependencies, but it seems more reliable to do it manually
pip3 install pillow requests

pip3 install --no-cache-dir turandot[tk,optional]
pip3 freeze > /tmp/turandotbuild/requirements.txt
deactivate
