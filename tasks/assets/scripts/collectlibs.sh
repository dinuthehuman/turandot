python{PYMAJ}.{PYMIN} -m venv {VENVDIR}/venv
. {VENVDIR}/venv/bin/activate
pip3 install turandot[gtk,optional]
pip3 wheel turandot[gtk,optional]
pip3 download {NONWHL}
deactivate
