# Strait-forward bash installer for Turandot

## Use test distribution on test.pypi.org?
TESTINDEX=0

# check if run as root
if [ "$EUID" -ne 0 ]
    then >&2 echo "ERROR: Please run me as root"
    exit 1
fi

# Determine distribution
echo "Determining distro/installed package manager..."

let DISTRO="none"

which apt
if [ $? -eq 0 ]; then
    DISTRO="debian"
    echo "apt is installed, probably a Debian based distro"
fi

which dnf
if [ $? -eq 0 ]; then
    DISTRO="fedora"
    echo "dnf is installed, probably a Fedora based distro"
fi

if [ $DISTRO == "none" ]; then
    >&2 echo "ERROR: Neither apt nor dnf is installed. This script is intended for Fedora or Debian-based distributions. Please install manually."
    exit 1
fi

# Dependency installation

echo "Installing dependencies..."

## RedHat/Fedora path
if [ $DISTRO == "fedora" ]; then
    dnf install -y python3 cairo-devel pkg-config python3-devel python3-gobject python3-gobject-devel cairo-gobject-devel
fi

## Debian path
if [ $DISTRO == "debian" ]; then
    apt-get install -y python3.10 python3.10-dev python3.10-venv python3.10-distutils libgirepository1.0-dev gcc libcairo2-dev pkg-config python3-dev libgtk-4-dev
fi

if [ $? -ne 0 ]; then
    echo "ERROR: Dependency installation failed"
    exit 1
fi

# Create folder structure

echo "Installing application..."

mkdir -p /opt/turandot
cd /opt/turandot


# Create venv

## RedHat/Fedora path
if [ $DISTRO == "fedora" ]; then
    python3 -m venv venv
fi

## Debian path
if [ $DISTRO == "debian" ]; then
    python3.10 -m venv venv
fi

if [ $? -ne 0 ]; then
    echo "ERROR: Creating virtual environment failed"
    exit 1
fi

# Install packages

. venv/bin/activate

if [ $TESTINDEX -eq 0 ]; then
    pip3 install turandot[gtk]
else
    pip3 install --no-cache-dir --index-url https://test.pypi.org/simple --extra-index-url https://pypi.org/simple turandot[gtk]
fi


if [ $? -ne 0 ]; then
    echo "ERROR: pip-installing application failed"
    exit 1
fi

# Deploy assets

python3 -m turandot.assets.extract -f turandot.svg -d /opt/turandot
python3 -m turandot.assets.extract -f turandot.sh -d /opt/turandot
python3 -m turandot.assets.extract -f update.sh -d /opt/turandot
python3 -m turandot.assets.extract -f uninstall.sh -d /opt/turandot
python3 -m turandot.assets.extract -f turandot.desktop -d /usr/share/applications

echo "Done!"
