# CSS Reference

## References/Citations

### Endnotes

Saved in Turandot config dir, in SQLite db `config.db`, table `reftmpl`, keys `endnotes_entries` and `endnotes_references`.

`div.endnotes_container`:
Wrapper around all endnotes entries

`div.endnote_entry`:
Wrapper around every single endnote entry in `div.endnotes_container`

`a.endnote_entryno`:
Reference number of each `div.endnote_entry`

`span.endnote_ref`:
Reference number in full text, containing `a` tag

### Inline References

Saved in Turandot config dir, in SQLite db `config.db`, table `reftmpl`, key `inline_entries`.

`span.inline_entry`:
Each single inline entry, brackets are hardcoded in template (move to CSS, maybe?)
