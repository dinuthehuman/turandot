# Turandot

**The documentation is a work in progress, please stand by for updates.**

Feature-rich, cross-platform graphical document converter


## Features

- Conversion from Markdown to PDF (extendable architecture for more formats)
- Graphical user interface in GTK (Linux) or Tk (Windows, MacOS)
- Layout and styling in CSS with extensive support of CSS3 printed page rules thanks to [Weasyprint](https://weasyprint.org/)
- Extensive templating capabilities based on [Mako](https://www.makotemplates.org/) and [Jinja](https://jinja.palletsprojects.com)
- Automatic handling of bibliographic references in combination with [Zotero](https://www.zotero.org/) or CSLJSON files with formatting based on CSL
- Rendering of mathematical formulas based on [KaTeX](https://katex.org/)
- Automatic syntax highlighting in code blocks based on [pygments](https://pygments.org/)
- Available as deb package with a Windows installer in progress

## Links

- [Source Code](https://gitlab.com/dinuthehuman/turandot)
- [Issue Tracker](https://gitlab.com/dinuthehuman/turandot/-/issues)
