from turandot.ui.frontend import TurandotFrontend
from turandot.ui.utils import FrontendUtils
from turandot.ui.background import background
from turandot.ui.exceptioncatcher import ExceptionCatcher, CatcherStrategy, catch_exception
from turandot.ui.enumtranslations import EnumTranslations


def i18n(s: str) -> str:
    return s
